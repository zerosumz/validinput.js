/**
 * 커스텀 다이얼로그 박스를 위한 벨리데이션 플러그인
 * 
 * usage
 * <pre>
 * $('input#foo').validInput([ [ /&circ;\d+$/, &quot;숫자여야지&quot; ], function(x) {
 * 	if (x % 2 != 0)
 * 		throw &quot;짝수만 됨&quot;
 * } ]);
 * </pre>
 * 
 * @author 장유현
 * 
 * @param tests
 *            함수 혹은 에러메세지와 짝을 이루는 함수 혹은 정규식 배열
 * @returns 모든 벨리데이션 체크에 통과하면 void 아니면 false
 * 
 */
$.setValidationDialog = function(alert){
	if (_.isFunction(alert)){
		dialogFunction = function () { alert.apply(window, arguments); }; 
	} else {
		dialogFunction = function(){ return; }
	}
	
	$.validationDialog = dialogFunction;
}

$.setValidationDialog(alert);

$.fn.extend({
	validInput : function(tests) {
		var $input = this;
		var form = this[0].form;
		$(form).submit(function(event) {
			try {
				_(tests).each(function(test) {
					var value = $input.val();
					if (_.isFunction(test)) {
						test(value);
					} else if (_.isArray(test)) {
						var testTerm = test[0];
						var exceptionMessage = test[1];
						if( (_.isRegExp(testTerm) && !(testTerm.test(value)))
								||  (_.isFunction(testTerm) && !(testTerm(value))) 
								|| 	(_.isBoolean(testTerm) && !testTerm) )
							throw exceptionMessage;
					} else {
						throw "스크립트 오류";
					}
				}, this);
			} catch (e) {
				event.stopImmediatePropagation();
				event.preventDefault();
				$.validationDialog(e);
				$input.focus();
			}
		});
	},
	
	validForm : function(){
		this.find('input').each(function(idx, el){
			var $el = $(el);
			$el.validInput(eval($el.data("valid")));
		});
	}
});